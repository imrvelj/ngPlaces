angular.module('wish', [])
  .controller('wishCtrl', function($scope, $window){
    if($window.localStorage.getItem('wishes') === ''){
      $scope.wishes = null;
    }
    else{
      storage = $window.localStorage.getItem('wishes');
      $scope.wishes = storage.split(';');
    }
    $scope.removeWish = function(wish){
      var id = $scope.wishes.indexOf(wish);
      $scope.wishes.splice(id, 1);
      $window.localStorage.setItem('wishes', $scope.wishes.join(';'));
    };
  });
