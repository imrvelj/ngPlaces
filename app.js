var app = angular.module('app', ['ngCookies', 'angularModalService', 'wish', 'detail']);

app.service('Map', function($q, ModalService, $cookies, $document, $window) {
    this.init = function($cookies) {
        var zg = new google.maps.LatLng(45.8167,15.9833);
        this.map = new google.maps.Map(
            document.getElementById("map"), {center: zg, zoom: 16}
        );
        this.places = new google.maps.places.PlacesService(this.map);
        this.markers = [];
        this.detail = {};

    };
    this.trazi = function(str, filter) {
        var d = $q.defer();
        var map = this.map;
        var loc = map.getCenter();
        var bounds = map.getBounds();
        var swPoint = bounds.getSouthWest();
        var long = swPoint.lng();
        var lat = loc.lat();

        var tocka = new google.maps.LatLng(lat,long);
        var radius = google.maps.geometry.spherical.computeDistanceBetween(loc, tocka);
        var request = {
          bounds: bounds,
          keyword: str,
          types: [filter]
        };
        this.places.radarSearch(request, function(results, status) {
            if (status == 'OK') {
                d.resolve(results);
            }
            else if (status == 'ZERO_RESULTS') {
              alert('No results!');
            }
            else d.reject(status);
        });
        return d.promise;
    };

    this.addMarker = function(res, id) {
        var marker = new google.maps.Marker({
          map: this.map,
          position: res.geometry.location,
          animation: google.maps.Animation.DROP,
          icon: 'https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678111-map-marker-32.png'
        });
        this.markers.push(marker);
        var request = {
          placeId: id
        };
        this.places.getDetails(request, function(place, status){
          var funkcija = function(){
            var mjesto = place;
            google.maps.event.addListener(marker, 'click', function() {
              try{
                  mjesto.open = place.opening_hours.periods[new Date().getDay()].open.time;
                  mjesto.close = place.opening_hours.periods[new Date().getDay()].close.time;
                  }
                  catch(e){
                    mjesto.open = 'Not found!';
                  }
              ModalService.showModal({
                templateUrl: 'detail.html',
                controller: 'detailCtrl'
              }).then(function(modal) {
                    modal.scope.detail = mjesto;
                    modal.element.modal();

                });

            });
          };
          if(status == 'OK'){
            $window.setTimeout(funkcija, 250);
          }
        });

    };
    this.removeMarkers = function(){
      for(var i=0; i < this.markers.length; i++){
        this.markers[i].setMap(null);
    }
    this.markers = [];
    };

}); //service

app.controller('dashCtrl',function($window, $scope, Map, $cookies, ModalService) {
    if($window.localStorage.getItem('wishes') === null){
      $window.localStorage.setItem('wishes', '');
      console.log('Bio je null!');
    }
    $scope.filters = [
      {name: 'Restoran', val: 'restaurant'}, {name: 'Kafić', val: 'cafe'}, {name: 'Klub', val: 'night_club'}, {name: 'Kino', val: 'movie_theater'}
  ];
    $scope.showWish = function(){
      ModalService.showModal({
        templateUrl: 'wishlist.html',
        controller: 'wishCtrl'
      }).then(function(modal) {
            modal.element.modal();

        });
    };
    $scope.place = {};
    var map = document.getElementById('map');
    $scope.setmaster = function(filter){
      $scope.selekt = '';
      $scope.selekt = filter;
      document.getElementById('text').innerHTML = filter.name + '<span class = "caret"></i>';
    };
    $scope.isset = function(filter){
      return $scope.selekt === filter;
    };
    $scope.trazi = function() {
        $scope.apiError = false;
        Map.trazi($scope.kveri, $scope.selekt.val)
        .then(
            function(res) { // success
              console.log('trazi.res.name: ' + res.place_id);
              Map.removeMarkers();
              var i = 0;
              function polako(){

              $scope.place[i] = res[i];
              Map.addMarker($scope.place[i], $scope.place[i].place_id);
              if(++i == res.length){
                return;
              }
              $window.setTimeout(polako, 300);
              }
              polako();
            },
            function(status) { // error
                console.log(status);
            }
        );
    };

    Map.init();
    //Map.addCircle();
});
