module.exports = function(grunt){
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      files: ['Gruntfile.js', '*.js'],
      options: {
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }
    },
    connect: {
    server: {
      options: {
        port: 3000,
        base: '.'
      }
    }
  },
    watch: {
      files: ['<%= jshint.files %>', '*.js'],
      tasks: ['jshint']
    }
  });


  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('serve', [
    'connect:server',
    'watch'
  ]);
  grunt.registerTask('default', console.log('GRUNT!'));

};
