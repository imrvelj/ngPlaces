var detail = angular.module('detail', ['angularModalService', 'app'])
  .controller('detailCtrl', function($scope, Map, $document, $window){

    $scope.save = function(){
      var wishes = [];
      var storage = '';

      if($window.localStorage.getItem('wishes') === ''){
        wishes = [];
      }
      else{
        storage = $window.localStorage.getItem('wishes');
        wishes = storage.split(';');
      }
      //if exist test
      for(var i = 0; i<wishes.length; i++){
        if(wishes[i] === $scope.detail.name){
          alert("Already in wishlist!");
          return;
        }
      }
      wishes.push($scope.detail.name);
      $window.localStorage.setItem('wishes', wishes.join(';'));
      $scope.added = true;

    };
  });
